<#include "base.ftl"/>
<#assign image_path="../upload/">
<!DOCTYPE html>
<html lang="en">

<head>
<@head/>
    <link rel="stylesheet" href="css/listofcosplay.css">
    <link rel="stylesheet" href="css/listofbatle.css">
</head>

<body>
<@nav/>

<div class=" container body">
    <h2>Свежие батлы</h2>
    <div style="min-height: 550px">
        <div class="row-fluid">
            <ul class="thumbnails">
                <#list battle_list as battle>
                    <li class="cosplay">
                        <div class="thumbnail">
                            <div class="row">
                                <div><img src="${image_path}${battle.getLeftCosplay().getImage()}" alt="">
                                    <img src="${image_path}${battle.getRightCosplay().getImage()}" alt=""></div>
                            </div>

                            <div class="row">
                                <div class="caption">
                                    <h3>${battle.getHeader()}</h3>
                                    <div class="col-md-6"><p>${battle.getLeftCosplay().getDescription()}</p>
                                    </div>
                                    <div class="col-md-6"><p><span>${battle.getRightCosplay().getDescription()}</span>
                                    </p></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <p><a href="/battles?id=${battle.getId()}" class="btn">Подробнее</a></p></div>
                            </div>
                        </div>
                    </li>
                </#list>

            </ul>
        </div>
    </div>
</div>

<@footer/>
<@scripts/>
</body>

</html>