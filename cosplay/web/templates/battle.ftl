<#include "base.ftl"/>
<#assign user_image="../upload/">
<!DOCTYPE html>
<html lang="en">

<head>
<@head/>
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/batle.css">
    <link rel="stylesheet" href="css/cosplaypage.css">
</head>
<body>
<@nav/>

<div class="container body">
    <h2>Батл</h2>
    <div style="min-height: 350px">
        <div class="row" style="margin-left:5px;">
            <div class="col-md-5 ">
                <img src="${user_image}${battle.getLeftCosplay().getImage()}" alt="Левыый косплей" >
            </div>
            <div class="col-md-2 vs">
                <img class="vs" src="img/vs.png" alt="" style="">
            </div>
            <div class="col-md-5">
                <img src="${user_image}${battle.getRightCosplay().getImage()}" alt="Правый косплей">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-5 " id="radiocb" onclick="cbclick(event)">
            <label for="">рейтинг: ${battle.getLeftCosplay().getRating()}</label>
            <div>
                <input type="checkbox" id="1_voted" onchange="fun1()" name="1_voted" value="1_member">Выбрать
            </div>
        </div>
        <div class="col-md-5 col-md-offset-2">
            <label for="">рейтинг: ${battle.getRightCosplay().getRating()}</label>
            <div>
                <input type="checkbox" id="2_voted" name="2_voted" value="2_member">Выбрать
            </div>
        </div>
    </div>

    <div class="row">
        <input type="submit" class="btn btn-default center-block" value="Проголосовать">
    </div>

    <h3>Комментарии</h3>
    <hr noshade size="14px">

    <#if flag>
    <div class="col-md-8 col-md-offset-2" style="margin-top:5px;">

        <input name="user_id" id="user_id" style="display: none" value= ${user_id}>
        <input name="block_id" id="block_id" style="display: none" value= ${block_id}>
        <input name="battle_id" style="display: none" value= ${battle_id}>

        <textarea name="comment" id="comment" placeholder="Оставьте свой комментарий"></textarea>
    </div>
    <button onclick="f()" class="proceed__button btn submit" aria-label="Опубликовать">
        <span class="glyphicon glyphicon-send"></span>
    </button>
    </#if>

    <div class="clearfix"></div>
    <div id="results">
    <#if comment_list?size == 0>
        <div class="col-md-8 col-md-offset-2">
            <p class="text-center" style="word-wrap: break-word;">Пока ни одного комментария не отправлено </p>
        </div>
    </#if>
    <#list comment_list as comment>
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><a href="#">${comment.getUsername()}</a></div>
                <div class="panel-body">
                    <p style="word-wrap: break-word;">${comment.getContent()}</p>
                </div>
            </div>
        </div>
    </#list>
    </div>
</div>
<@footer/>
<@scripts/>


<script type="application/javascript">
    function fun1() {
        var $unique = $('input[type="checkbox"]');
        $unique.click(function () {
            $unique.filter(':checked').not(this).removeAttr('checked');
        });
    }

    function f() {
        $.ajax({
            url: "/comment",
            "data": {
                "user_id": $("#user_id").val(),
                "block_id": $("#block_id").val(),
                "is_battle": true,
                "comment": $("#comment").val()
            },
            "dataType": "json",
            success: function (result) {
                $("#results").html("");
                $("#comment").val("");
                if (result.data.length == 0) {
                    $("#results").append("Пока ни одного комментария не отправлено");
                }
                for (var i = 0; i < result.data.length; i++) {
                    $("#results").append("<div class=\"col-md-8 col-md-offset-2\">\n" +
                            "                <div class=\"panel panel-default\">\n" +
                            "                    <div class=\"panel-heading\"><a href=\"#\">" + result.data[i].username + "</a></div>\n" +
                            "                    <div class=\"panel-body\">\n" +
                            "                        <p>" + result.data[i].content + "</p>\n" +
                            "                    </div>\n" +
                            "                </div>\n" +
                            "            </div>");
                }
            },
            error: function (msg) {
                alert("OOOOps");
            }
        });
    }
</script>

</body>
</html>