<#include "base.ftl"/>
<#assign cosplay_image="../upload/${cosplay.getImage()}">
<!DOCTYPE html>
<html lang="ru">

<head>
<@head/>
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/cosplaypage.css">
    <link rel="stylesheet" href="owlcarousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="owlcarousel/owl.theme.green.css">
    <link rel="stylesheet" href="owlcarousel/owl.carousel.min.css">
</head>

<body>
<@nav/>


<div class=" container body">

    <h2>
    ${cosplay.getHeader()}
    </h2>
    <div style="min-height: 550px">
        <div class="row description">
            <div class="col-md-6">
                <img src="${cosplay_image}" alt="">
            </div>
            <div class="col-md-6">
                <br/>
                <p>${cosplay.getDescription()}</p>
            </div>
            <div class="col-md-5 tags">
                <p>
                <#list tags as t>
                ${t},
                </#list>
                </p>
            </div>
        </div>
        <!--
        <div class="row" style="margin-top: 10px ">
            <div class="col-md-5" id="reviewStars-input">
                <input id="star-9" type="radio" name="reviewStars"/>
                <label title="10" for="star-9"></label>

                <input id="star-8" type="radio" name="reviewStars"/>
                <label title="9" for="star-8"></label>

                <input id="star-7" type="radio" name="reviewStars"/>
                <label title="8" for="star-7"></label>

                <input id="star-6" type="radio" name="reviewStars"/>
                <label title="7" for="star-6"></label>

                <input id="star-5" type="radio" name="reviewStars"/>
                <label title="6" for="star-5"></label>

                <input id="star-4" type="radio" name="reviewStars"/>
                <label title="5" for="star-4"></label>

                <input id="star-3" type="radio" name="reviewStars"/>
                <label title="4" for="star-3"></label>

                <input id="star-2" type="radio" name="reviewStars"/>
                <label title="3" for="star-2"></label>

                <input id="star-1" type="radio" name="reviewStars"/>
                <label title="2" for="star-1"></label>

                <input id="star-0" onclick="star()" value="1" type="radio" name="reviewStars"/>
                <label title="1" for="star-0"></label>
            </div>
            -->
    <#if flag>
        <div class="col-md-4" style="margin-top:10px">
            <input type="number" pattern="[0-10]" name="mark" id="mark">
            <button onclick="star()">Отправить</button>
            <label id="error"></label>
        </div>
    </#if>

        <div class="col-md-4 " style="margin-top:10px">
            <p class="marks" id="marks">
                <!--Средняя оценка -->
            ${cosplay.getRating()}/10
            </p>
        </div>
        <div class="col-md-4" style="margin-top:10px">
            <p class="voted" id="voted">
                <!--кол-во проголосовавших -->
            ${cosplay.getVotesCount()} голосов
            </p>
        </div>
    <#if flag>
        <div class="col-md-2 " style="margin-top:10px">
            <form action="/battles" method="post">
                <input name="cosplay_id" id="cosplay_id" style="display: none" value= ${cosplay_id}>
                <input name="cosplay_name" id="cosplay_name" style="display: none" value= ${cosplay.getHeader()}>
                <input type="submit" value="Батл" class="btn btn-default" style="margin:0"/>
            </form>
        </div>
    </#if>
    </div>
    <h3>Комментарии</h3>
    <hr noshade size="14px">

<#if flag>
    <div class="col-md-8 col-md-offset-2">
        <input name="user_id" id="user_id" style="display: none" value= ${user_id}>
        <input name="block_id" id="block_id" style="display: none" value= ${block_id}>
        <input name="cosplay_id" id="cosplay_id" style="display: none" value= ${cosplay_id}>
        <textarea name="comment" id="comment" placeholder="Оставьте свой комментарий"></textarea>
    </div>
    <button onclick="f()" class="proceed__button btn submit" aria-label="Опубликовать">
        <span class="glyphicon glyphicon-send"></span>
    </button>
</#if>

    <div class="clearfix"></div>

    <div id="results">
        <#if comment_list?size == 0>
        <div class="col-md-8 col-md-offset-2">
            <p class="text-center" style="word-wrap: break-word;">Пока ни одного комментария не отправлено </p>
        </div>
        </#if>
    <#list comment_list as comment>
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><a href="#">${comment.getUsername()}</a></div>
                <div class="panel-body">
                    <p style="word-wrap: break-word;">${comment.getContent()}</p>
                </div>
            </div>
        </div>
    </#list>
    </div>
</div>
</div>


<@footer/>

<@scripts/>

<script type="application/javascript">
    function f() {
        $.ajax({
            url: "/comment",
            "data": {
                "user_id": $("#user_id").val(),
                "block_id": $("#block_id").val(),
                "comment": $("#comment").val()
            },
            "dataType": "json",
            success: function (result) {
                $("#results").html("");
                $("#comment").val("");
                for (var i = 0; i < result.data.length; i++) {
                    if (result.data.length == 0) {
                        $("#results").append("Пока ни одного комментария не отправлено");
                    }
                    $("#results").append("<div class=\"col-md-8 col-md-offset-2\">\n" +
                            "                <div class=\"panel panel-default\">\n" +
                            "                    <div class=\"panel-heading\"><a href=\"#\">" + result.data[i].username + "</a></div>\n" +
                            "                    <div class=\"panel-body\">\n" +
                            "                        <p>" + result.data[i].content + "</p>\n" +
                            "                    </div>\n" +
                            "                </div>\n" +
                            "            </div>");
                }
            },
            error: function (msg) {
                alert("OOOOps");
            }
        });
    }

    function star() {
        $.ajax({
            url: "/cosplay-like",
            "data": {
                "user_id": $("#user_id").val(), "cosplay_id": $("#cosplay_id").val(),
                "mark": $("#mark").val()
            },
            "dataType": "json",
            success: function (result) {
                $("#error").html("");
                if (result.data == "OK") {
                    alert("I am here");
                    $("#marks").html((Number)(result.rating) + "/10");
                    $("#voted").html(((Number)(result.counter)) + " голосов");
                }
                else
                    $("#error").append("Вы уже голосовали");
            },
            error: function (msg) {
                alert("OOOOps");
            }
        });
    }
</script>

<script src="owlcarousel/owl.carousel.min.js"></script>

<script>
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        autoplay: true,
        smartSpeed: 1000, //Время движения слайда
        autoplayTimeout: 3000, //Время смены слайда
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            }
        }
    })
</script>

</body>
</html>