<#include "base.ftl"/>
<!DOCTYPE html>
<html lang="en">

<head>
<@head/>
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/checkin.css">
</head>

<body>
<@nav/>


<div class="container body">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h2>
                Регистрация
            </h2>
            <div class="forms" style="min-height: 550px">


                <form id="form_test" method="post">
                    <div class="row">
                        <label class="label_desc">Логин</label>
                        <input type="text" name="login" id="name_user" required autofocus>
                    </div>
                    <div class="row">
                        <label style="font-size: 9pt" id="name_user_error" class="error"></label>
                    </div>

                    <div class="row">
                        <label class="label_desc">Имя</label>
                        <input type="text" name="username" required>
                    </div>
                    <div class="row">
                        <label class="label_desc">Email</label>
                        <input type="email" name="email" id="email_user">
                    </div>
                    <div class="row">
                        <label style="font-size: 9pt" id="email_user_error" class="error"></label>
                    </div>

                    <div class="row">
                        <label class="label_desc">Пароль</label>
                        <input type="password" name="password" id="password_user" required>
                    </div>
                    <div class="row">
                        <label style="font-size: 9pt" id="password_user_error" class="error"></label>
                    </div>

                    <div class="row">
                        <label class="label_desc">Повторите пароль</label>
                        <input type="password" name="password_repeat" id="password_2_user" required>
                    </div>
                    <div class="row">
                        <label style="font-size: 9pt" id="password_2_user_error" class="error"></label>
                    </div>

                    <input type="submit" class="btn btn-default center-block" value="Сохранить" id="send_data">
                </form>

            </div>
        </div>
    </div>
</div>


<@footer/>


<@scripts/>


<script type="application/javascript">
    $(document).ready(function () {
        $('#form_test').submit(function () {
            // убираем класс ошибок с инпутов
            $('input').each(function () {
                $(this).removeClass('error_input');
            });
            // прячем текст ошибок
            $('.error').hide();

            // получение данных из полей
            var name_user = $('#name_user').val();
            var email_user = $('#email_user').val();
            var password_user = $('#password_user').val();
            var password_2_user = $('#password_2_user').val();
            var flag = false;
            $.ajax({
                // метод отправки
                type: "POST",
                // путь до скрипта-обработчика
                url: "/ajax-checkin",
                // какие данные будут переданы
                data: {
                    'name_user': name_user,
                    'email_user': email_user,
                    'password_user': password_user,
                    'password_2_user': password_2_user
                },
                // тип передачи данных
                dataType: "json",
                // действие, при ответе с сервера
                success: function (data) {
                    // в случае, когда пришло success. Отработало без ошибок
                    if (data.result == 'success') {
                        console.log('форма корректно заполнена')
                        // в случае ошибок в форме
                    } else {
                        console.log('форма некорректно заполнена');
                        flag = true;
                        // перебираем массив с ошибками
                        for (var errorField in data.text_error) {
                            // выводим текст ошибок
                            $('#' + errorField + '_error').html(data.text_error[errorField]);
                            // показываем текст ошибок
                            $('#' + errorField + '_error').show();
                            // обводим инпуты красным цветом
                            $('#' + errorField).addClass('error_input');
                        }
                    }
                }
            });
            // останавливаем сабмит, чтоб не перезагружалась страница
            if (flag) {
                return false;
            }
        });
    });
</script>
</body>
</html>
