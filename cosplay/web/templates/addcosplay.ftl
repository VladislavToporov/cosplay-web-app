<#include "base.ftl"/>
<!DOCTYPE html>
<html lang="en">

<head>
<@head/>
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/checkin.css">
    <link rel="stylesheet" href="css/addcosplay.css">

</head>
<body>
<@nav/>

<div class="container body">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h2>Добавление косплея</h2>
            <div class="forms" style="min-height: 550px">
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <label>Заголовок</label>
                        <input type="text" name="header">
                    </div>
                    <div class="row">
                        <label>Описание</label>
                        <textarea name="description" id="description" placeholder="Не больше 300 символов" ></textarea>
                    </div>
                    <div class="row">
                        <input type="file" name="data">
                    </div>
                    <input type="submit" class="btn btn-default center-block" value="Добавить">
                </form>
            </div>
        </div>
    </div>
</div>

<@footer/>

<@scripts/>
</body>
</html>