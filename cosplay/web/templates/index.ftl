<#include "base.ftl"/>
<#assign image_path="../upload/">
<!DOCTYPE html>
<html lang="en">

<head>
<@head/>
    <link rel="stylesheet" href="css/index.css">
</head>

<body>
<@nav/>

<div class=" container body">
    <h2>
        Самое свежее
    </h2>

    <textarea id = "offset" style="display: none">${offset}</textarea>
    <div style="min-height: 550px">
        <div class="row-fluid">
            <ul class="thumbnails" id="results">
            <#list cosplay_list as cosplay>
                <li class="cosplay">
                    <div class="thumbnail">
                        <img src="${image_path}${cosplay.getImage()}" alt="Косплей" data-src="holder.js/300x200"
                             style="width: auto; height: auto;">
                        <div class="caption">
                            <h3>${cosplay.getHeader()}</h3>
                            <p>${cosplay.getDescription()}</p>
                            <p><a href="/cosplays?id=${cosplay.getId()}" class="btn">Подробнее</a></p>
                        </div>
                    </div>
                </li>
            </#list>

            </ul>
        </div>
    </div>
    <button type="button" class="btn btn-default" onclick="f()">Еще</button>
</div>
<#if flag>
    <a href="cosplays-add"><input type="image" src="img/add_77928.png" name=""/></a>
</#if>

<@footer/>

<@scripts/>

<script type="application/javascript">
    function f() {
        $.ajax({
            url: "/cosplay-ajax",
            "data": {"offset": $("#offset").val()},
            "dataType": "json",
            success: function (result) {
                for (var i = 0; i < result.data.length; i++) {
                    $("#results").append(" <li class=\"cosplay\">\n" +
                            "                    <div class=\"thumbnail\">\n" +
                            "                        <img src=\"${image_path}" + result.data[i].image + "\" alt=\"300x200\" data-src=\"holder.js/300x200\"\n" +
                            "                             style=\"width: auto; height: auto;\">\n" +
                            "                        <div class=\"caption\">\n" +
                            "                            <h3>" + result.data[i].header + "</h3>\n" +
                            "                            <p>" + result.data[i].description + "</p>\n" +
                            "                            <p><a href=\"/cosplays?id=" + result.data[i].id + "\" class=\"btn\">Подробнее</a></p>\n" +
                            "                        </div>\n" +
                            "                    </div>\n" +
                            "                </li>");
                }
                $('#offset').val((Number)(result.offset));
            },
            error: function (msg) {
                alert("OOOOps");
            }
        });
    }
</script>
</body>
</html>