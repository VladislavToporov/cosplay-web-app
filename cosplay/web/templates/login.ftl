<#include "base.ftl"/>
<!DOCTYPE html>
<html lang="en">

<head>
<@head/>
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/checkin.css">
</head>

<body>
<@nav/>


<div class="container body">
    <div class="row">
        <div class="col-md-6 col-md-offset-4">
            <h2 style="margin-right: 50px">
                Вход
            </h2>
        <#if fail>
            <div class="text-center">Не зарегистрировано пользователя с таким логином и паролем</div>
        </#if>
            <div class="forms" style="min-height: 550px">
                <form action="" method="post">
                    <div class="row">
                        <label>Логин</label>
                        <input type="text" name="username">
                    </div>
                    <div class="row">
                        <label>Пароль</label>
                        <input type="password" name="password">
                    </div>
                    <p class="checkbox center-block">
                        <input type="checkbox" name="remember_me" value="1" checked> Запомнить меня
                    </p>
                    <input type="submit" class="btn btn-default center-block" value="Войти"
                           style="margin:10px 0 0 120px!important">
                </form>
            </div>
        </div>
    </div>
</div>


<@footer/>

<@scripts/>
</body>
</html>