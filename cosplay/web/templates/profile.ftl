<#include "base.ftl"/>
<#assign user_image="../upload/${user.getImage()}">
<!DOCTYPE html>
<html lang="en">

<head>
    <@head/>
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/profil.css">
</head>

<body>
    <@nav/>
    <div class="container body">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <h2>${user.getUsername()}</h2>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-5 col-md-offset-1">
            <#if user.getImage() != "">
                    <img src="${user_image}" alt="Аватар">
            </#if>
            <#if user.getImage() == "">
                <img src="../upload/avatar_default.jpg" alt="Аватар">
            </#if>

            </div>
            <div class="col-md-6 list-of-cosplay">
                <#list cosplay_list as cosplay>
                    <a href="cosplays?id=${cosplay.getId()}">${cosplay.getHeader()}</a>
                </#list>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-1 rating">
                <p>Ваш рейтинг</p>
                <p class="rating">
                    ${user.getRating()}
                </p>
            </div>
        </div>

    </div>
    <@footer/>
    <@scripts/>
</body>
</html>