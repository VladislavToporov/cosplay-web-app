<#include "base.ftl"/>
<#assign user_image="../upload/">
<!DOCTYPE html>
<html lang="en">

<head>
    <@head/>
    <link rel="stylesheet" href="css/listofcosplay.css">
    <link rel="stylesheet" href="css/listofprofils.css">
</head>

<body>

    <@nav/>

    <div class=" container body">
        <div style="min-height: 550px">
            <h2>Участники</h2>
            <div class="row-fluid">
                <ul class="thumbnails">
                    <#list list_profiles as profile>
                    <li class="cosplay">
                        <div class="container-fluid thumbnail">
                           <a href="/profiles?id=${profile.getId()}">
                           <div class="col-md-5">
                               <#if profile.getImage() != "">
                            <img src="${user_image}${profile.getImage()}" alt="Аватар">
                               </#if>
                               <#if profile.getImage() == "">
                                   <img src="${user_image}avatar_default.jpg" alt="Аватар2">
                               </#if>
                            </div>
                            <div class="col-md-6 col-md-offset-1 caption">
                                <h3>${profile.getUsername()}</h3>
                                <p id="rating">Рейтинг ${profile.getRating()}</p>
                            </div>
                            </a>
                        </div>
                    </li>
                    </#list>
                </ul>
            </div>
        </div>
    </div>
    <@footer/>
    <@scripts/>
</body>
</html>