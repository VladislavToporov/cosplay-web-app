<#macro head>
<meta charset="UTF-8">
<title>EasyCosplay</title>
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css">
<link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
</#macro>


<#macro nav>
<header>
    <nav class="navbar navbar-light" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index">EasyCosplay</a>
            </div>
            <div class="collapse navbar-collapse" id="">
                <ul class="nav navbar-nav">
                    <li><a href="index">Главная</a></li>
                    <li><a href="cosplays">Косплеи</a></li>
                    <li><a href="battles">Батлы</a></li>
                    <li><a href="profiles">Участники</a></li>
                </ul>


                <form id="myform" action="/index" method="post" style="display:none;">
                </form>

                <ul class="nav navbar-nav navbar-right ">
                    <#if flag>
                        <!-- <li><a href="#">Личные сообщения</a></li> -->
                        <li><a role="button" name="logout"
                               onclick='document.getElementById("myform").submit();'>Выйти</a></li>
                    <#else>
                        <li><a href="login">Войти</a></li>
                        <li><a href="checkin">Регистрация</a></li>
                    </#if>
                </ul>
            </div>
        </div>
    </nav>
</header>
</#macro>

<#macro footer>
<footer class="footer">
    <div class="container">
        <p> &copy 2017</p>
    </div>
</footer>
</#macro>

<#macro carousel>
<div class="container body">
    <div class="owl-carousel owl-theme" style="margin-left: 25px">
        <div class="item"><img src="img/1449358714174652515.jpg"></div>
        <div class="item"><img src="img/Haski.jpg"></div>
        <div class="item"><img src="img/Haski.jpg"></div>
        <div class="item"><img src="img/Haski.jpg"></div>
        <div class="item"><img src="img/Haski.jpg"></div>
    </div>
</div>
</#macro>

<#macro scripts>
<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script type="application/javascript" src="bootstrap/js/jquery-3.2.1.js"></script>
<script type="application/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script type="application/javascript" src="bootstrap/js/bootstrap.min.js"></script>
</#macro>
