<#include "base.ftl"/>
<#assign image_path="../upload/">
<!DOCTYPE html>
<html lang="en">

<head>
<@head/>
    <link rel="stylesheet" href="css/listofcosplay.css">
</head>

<body>
<@nav/>

<div class=" container body">
    <div style="min-height: 550px">

        <div class="search">
            <div class="col-md-7">
                <input type="text" class="search-query" id="query" placeholder="Search">
                <button type="button" onclick="f()" class="btn btn-default">Поиск</button>
            </div>

            <ul class="radio-button">
                <li>
                    <input type="radio" checked class="radio" id="title" name="selector" value="headerRadio">
                    <label for="title">По заголовкам</label>

                    <div class="check"></div>
                </li>

                <li>
                    <input type="radio" class="radio" id="tags" name="selector" value="tagRadio">
                    <label for="tags">По тегам</label>

                    <div class="check">
                        <div class="inside"></div>
                    </div>
                </li>
            </ul>

        </div>


        <div class="row-fluid">
            <ul class="thumbnails" id="results">
            <#list cosplay_list as cosplay>
                <li class="cosplay">
                    <div class="thumbnail">
                        <img src="${image_path}${cosplay.getImage()}" alt="Косплей" data-src="holder.js/300x200"
                             style="width: auto; height: auto;">
                        <div class="caption">
                            <h3>${cosplay.getHeader()}</h3>
                            <p>${cosplay.getDescription()}</p>
                            <p><a href="/cosplays?id=${cosplay.getId()}" class="btn">Подробнее</a></p>
                        </div>
                    </div>
                </li>
            </#list>

            </ul>
        </div>
    </div>
</div>

<@footer/>
<@scripts/>
<script type="application/javascript">
    console.log($("#title").attr("checked"));
    console.log($("#tags").attr("checked"));

    function f() {
        $.ajax({
            url: "/search",
            "data": {"query": $("#query").val(), "radio": $('input[name=selector]:checked').val()},
            "dataType": "json",
            success: function (result) {
                $("#results").html("");
                if (result.data.length == 0) {
                    $("#results").append("Не найдено записей по вашему запросу");
                }
                for (var i = 0; i < result.data.length; i++) {
                    $("#results").append(" <li class=\"cosplay\">\n" +
                            "                    <div class=\"thumbnail\">\n" +
                            "                        <img src=\"${image_path}" + result.data[i].image + "\" alt=\"300x200\" data-src=\"holder.js/300x200\"\n" +
                            "                             style=\"width: auto; height: auto;\">\n" +
                            "                        <div class=\"caption\">\n" +
                            "                            <h3>" + result.data[i].header + "</h3>\n" +
                            "                            <p>" + result.data[i].description + "</p>\n" +
                            "                            <p><a href=\"/cosplays?id=" + result.data[i].id + "\"" + " class=\"btn\">Подробнее</a></p>\n" +
                            "                        </div>\n" +
                            "                    </div>\n" +
                            "                </li>");
                }
            },
            error: function (msg) {
                alert("OOOOps");
            }
        });
    }
</script>
</body>
</html>
