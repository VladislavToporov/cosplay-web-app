package helpers;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class MD5Util {
    public static String md5Custom(String st) {
        //st += salt();
        MessageDigest messageDigest = null;
        byte[] digest = new byte[0];

        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(st.getBytes());
            digest = messageDigest.digest();
        } catch (NoSuchAlgorithmException e) {
            // тут можно обработать ошибку
            // возникает она если в передаваемый алгоритм в getInstance(,,,) не существует
            e.printStackTrace();
        }
        BigInteger bigInt = new BigInteger(1, digest);
        StringBuilder md5Hex = new StringBuilder(bigInt.toString(16));
        while( md5Hex.length() < 32 ){
            md5Hex.insert(0, "0");
        }
        return md5Hex.toString();
    }

    public static String salt() {
        Random r = new Random();
        int k = r.nextInt(10) + 1;
        StringBuilder res = new StringBuilder();
        int min = 'A';
        int max = 'z';
        for (int i = 0; i < k; i++) {
            res.append((char) (min + r.nextInt(max - min + 1)));
        }
        return res.toString();
    }

    public static void main(String[] args) {
        System.out.println(salt());
        String st = "2";
        System.out.println(md5Custom(salt()));
    }
}
