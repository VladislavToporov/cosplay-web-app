package helpers;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnectionSingleton {
    private static DBConnectionSingleton dbs = new DBConnectionSingleton();
    private static Connection conn;

    public static Connection getInstance() {
        return conn;
    }

    private DBConnectionSingleton() {
        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/easy_cosplay",
                    "easy_cosplay_owner",
                    "easy_cosplay_owner"
            );
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
}
