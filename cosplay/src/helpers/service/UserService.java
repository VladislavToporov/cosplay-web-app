package helpers.service;

import database.dao.DaoFactory;
import database.dao.GenericDao;
import database.dao.PersistException;
import database.models.Cosplay;
import database.models.User;
import database.sql.MyDaoFactory;
import helpers.MD5Util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class UserService {
    private GenericDao userDao;
    private Connection connection;

    public GenericDao getUserDao() {
        return userDao;
    }

    private static volatile UserService us;

    public static UserService getInstance() {
        UserService localInstance = us;
        if (localInstance == null) {
            synchronized (UserService.class) {
                localInstance = us;
                if (localInstance == null) {
                    us = localInstance = new UserService();
                }
            }
        }
        return localInstance;
    }

    private UserService() {
        try {
            DaoFactory<Connection> factory = new MyDaoFactory();
            connection = factory.getContext();
            this.userDao = factory.getDao(connection, User.class);
        } catch (PersistException e) {
            e.printStackTrace();
        }
    }

    public int checkPL(String login, String key, String type) {
        String sql = "SELECT EXISTS(SELECT user_profile.login FROM user_profile " +
                "WHERE login = ? AND " + type + " = ?);";
        try (PreparedStatement statement = connection.prepareStatement(sql);) {
            statement.setString(1, login);
            statement.setString(2, key);
            ResultSet rs = statement.executeQuery();
            rs.next();

            if (rs.getBoolean("exists")) {
                sql = "SELECT id FROM user_profile " +
                        "WHERE login = ?;";
                try (PreparedStatement st = connection.prepareStatement(sql);) {
                    st.setString(1, login);
                    ResultSet rs2 = st.executeQuery();
                    rs2.next();
                    return rs2.getInt("id");
                }
            }
        } catch (Exception e) {
            try {
                throw new PersistException(e);
            } catch (PersistException e1) {
                e1.printStackTrace();
            }
        }
        return -1;
        //return userDao.getUserByPL(connection, login, key, type);
    }

    public boolean checkKey(String key, String value) {
        String sql = "SELECT EXISTS(SELECT user_profile.id FROM user_profile " +
                "WHERE " + key + " = ?);";
        try (PreparedStatement statement = connection.prepareStatement(sql);) {
            statement.setString(1, value);
            ResultSet rs = statement.executeQuery();
            rs.next();
            return rs.getBoolean("exists");
        } catch (Exception e) {
            try {
                throw new PersistException(e);
            } catch (PersistException e1) {
                e1.printStackTrace();
            }
        }
        return false;
    }
    public void updateToken(String login, String token) {
        String sql = "UPDATE user_profile SET token = ? WHERE login = ?;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, token);
            ps.setString(2, login);
            ps.executeUpdate();
        } catch (Exception e) {
            try {
                throw new PersistException(e);
            } catch (PersistException e1) {
                e1.printStackTrace();
            }
        }
    }

    public void logout(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.getSession().removeAttribute("current_user");
        request.getSession().removeAttribute("id");
        clearCookie(request);
    }

    private void clearCookie(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        for (Cookie c : cookies) {
            if (c.getName().equalsIgnoreCase("login")) {
                c.setMaxAge(0);
            }
            if (c.getName().equalsIgnoreCase("token")) {
                c.setMaxAge(0);

            }
        }
    }

    public double avgCosplay(int userId) {
        double ans = 0;
        String sql = "SELECT avg(rating) from cosplay WHERE user_id = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            rs.next();
            ans = rs.getDouble(1);
        } catch (Exception e) {
            try {
                throw new PersistException(e);
            } catch (PersistException e1) {
                e1.printStackTrace();
            }
        }
        return ans;
    }

    public double avgComment(int userId) {
        double ans = 0;
        String sql = "SELECT avg(rating) from comment WHERE user_id = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            rs.next();
            ans = rs.getDouble(1);
        } catch (Exception e) {
            try {
                throw new PersistException(e);
            } catch (PersistException e1) {
                e1.printStackTrace();
            }
        }
        return ans;
    }


    public List<Cosplay> pullCosplay(Integer userId, int offset, int limit) {
        List<Cosplay> list = new ArrayList<>();
        String sql = "SELECT * FROM cosplay where user_id = ? ORDER BY rating DESC, date DESC, TIME DESC OFFSET ? LIMIT ?;";
        try (PreparedStatement statement = connection.prepareStatement(sql);) {
            statement.setInt(1, userId);
            statement.setInt(2, offset);
            statement.setInt(3, limit);
            ResultSet rs = statement.executeQuery();
            list = CosplayService.getInstance().parseResultSet(rs);
        } catch (Exception e) {
            try {
                throw new PersistException(e);
            } catch (PersistException e1) {
                e1.printStackTrace();
            }
        }
        return list;
    }

    public static void main(String[] args) {
        UserService u = new UserService();
        System.out.println(u.checkPL("vladislav", MD5Util.md5Custom("vlad"), "password"));
    }
}

