package helpers.service;

import database.dao.DaoFactory;
import database.dao.GenericDao;
import database.dao.PersistException;
import database.models.Cosplay;
import database.models.User;
import database.sql.CosplayDao;
import database.sql.MyDaoFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class CosplayService {
    private GenericDao cosplayDao;
    private Connection connection;

    public GenericDao getCosplayDao() {
        return cosplayDao;
    }
    private static volatile CosplayService cs;

    public static CosplayService getInstance() {
        CosplayService localInstance = cs;
        if (localInstance == null) {
            synchronized (CosplayService.class) {
                localInstance = cs;
                if (localInstance == null) {
                    cs = localInstance = new CosplayService();
                }
            }
        }
        return localInstance;
    }

    private CosplayService() {
        try {
            DaoFactory<Connection> factory = new MyDaoFactory();
            connection = factory.getContext();
            this.cosplayDao = factory.getDao(connection, Cosplay.class);
        } catch (PersistException e) {
            e.printStackTrace();
        }
    }

    protected List<Cosplay> parseResultSet(ResultSet rs) throws PersistException {
        HashTagService hashTagService = HashTagService.getInstance();
        UserService userService = UserService.getInstance();
        List<Cosplay> result = new ArrayList<>();
        try {
            while (rs.next()) {
                CosplayDao.PersistCosplay cosplay = new CosplayDao.PersistCosplay();
                cosplay.setId(rs.getInt("id"));
                cosplay.setUserId(rs.getInt("user_id"));
                cosplay.setUser((User) userService.getUserDao().getByPK(cosplay.getUserId()));
                cosplay.setBlockId(rs.getInt("comment_block_id"));
                cosplay.setAlbumId(rs.getInt("album_id"));
                cosplay.setHeader(rs.getString("header"));
                cosplay.setDescription(rs.getString("description"));
                cosplay.setVotesSum(rs.getInt("votes_sum"));
                cosplay.setVotesCount(rs.getInt("votes_counter"));
                cosplay.setImage(rs.getString("image"));
                List<String> tags = hashTagService.pullHashtag(cosplay.getId());
                cosplay.setTags(tags);
                result.add(cosplay);
            }
        } catch (Exception e) {
            throw new PersistException(e);
        }
        return result;
    }

    public List<Cosplay> pullCosplay(int offset, int limit, String querry, String param) {
        List<Cosplay> list = new ArrayList<>();
        String sql = "SELECT * FROM cosplay ORDER BY rating DESC, date DESC, TIME DESC OFFSET ? LIMIT ?;";
        int i = 1;
        if (querry != null)
            sql = querry;
        try (PreparedStatement statement = connection.prepareStatement(sql);) {
            if (querry != null)
                statement.setString(i++, param);
            statement.setInt(i++, offset);
            statement.setInt(i++, limit);
            ResultSet rs = statement.executeQuery();
            list = parseResultSet(rs);
        } catch (Exception e) {
            try {
                throw new PersistException(e);
            } catch (PersistException e1) {
                e1.printStackTrace();
            }
        }
        return list;

    }

    public int getMax() {
        String sql = "SELECT max(votes_counter) FROM cosplay;";
        try (PreparedStatement statement = connection.prepareStatement(sql);) {
            ResultSet rs = statement.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public boolean checkLike(int userId, int cosplayId) {
        int count = 0;
        String sql = "INSERT INTO cosplay_like (user_id, cosplay_id) VALUES (?, ?)  ON CONFLICT DO NOTHING;";

        try (PreparedStatement statement = connection.prepareStatement(sql);) {
            statement.setInt(1, userId);
            statement.setInt(2, cosplayId);
            count = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count == 1;
    }
}
