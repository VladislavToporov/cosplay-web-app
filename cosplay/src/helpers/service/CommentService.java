package helpers.service;

import database.dao.DaoFactory;
import database.dao.GenericDao;
import database.dao.PersistException;
import database.models.Comment;
import database.models.User;
import database.sql.CommentDao;
import database.sql.MyDaoFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class CommentService {
    private GenericDao commentDao;
    private Connection connection;

    public GenericDao getCommentDao() {
        return commentDao;
    }

    private static volatile CommentService cs;

    public static CommentService getInstance() {
        CommentService localInstance = cs;
        if (localInstance == null) {
            synchronized (CommentService.class) {
                localInstance = cs;
                if (localInstance == null) {
                    cs = localInstance = new CommentService();
                }
            }
        }
        return localInstance;
    }


    private CommentService() {
        try {
            DaoFactory<Connection> factory = new MyDaoFactory();
            connection = factory.getContext();
            this.commentDao = factory.getDao(connection, Comment.class);
        } catch (PersistException e) {
            e.printStackTrace();
        }
    }

    protected List<Comment> parseResultSet(ResultSet rs) throws PersistException {
        UserService userService = UserService.getInstance();
        List<Comment> result = new ArrayList<>();
        try {
            while (rs.next()) {
                CommentDao.PersistComment comment = new CommentDao.PersistComment();
                comment.setId(rs.getInt("id"));
                comment.setUserId(rs.getInt("user_id"));
                comment.setUser((User) userService.getUserDao().getByPK(comment.getUserId()));
                comment.setBlockId(rs.getInt("block_id"));
                comment.setBattleBlockId(rs.getInt("battle_block_id"));
                comment.setAlbumId(rs.getInt("album_id"));
                comment.setContent(rs.getString("content"));
                comment.setVotesSum(rs.getInt("votes_sum"));
                comment.setVotesCount(rs.getInt("votes_counter"));

                result.add(comment);
            }
        } catch (Exception e) {
            throw new PersistException(e);
        }
        return result;
    }

    public List<Comment> pullComment(String blockName, int blockId, int offset, int limit) {
        List<Comment> list = new ArrayList<>();
        String sql = "SELECT * FROM comment WHERE " + blockName + " = ?  ORDER BY rating DESC, date DESC, TIME DESC OFFSET ? LIMIT ?;";
        try (PreparedStatement statement = connection.prepareStatement(sql);) {
            statement.setInt(1, blockId);
            statement.setInt(2, offset);
            statement.setInt(3, limit);
            ResultSet rs = statement.executeQuery();
            list = parseResultSet(rs);
        } catch (Exception e) {
            try {
                throw new PersistException(e);
            } catch (PersistException e1) {
                e1.printStackTrace();
            }
        }
        return list;
    }


    public int getMax() {
        String sql = "SELECT max(votes_counter) FROM comment;";
        try (PreparedStatement statement = connection.prepareStatement(sql);) {
            ResultSet rs = statement.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public boolean checkLike(int userId, int commentId) {
        int count = 0;
        String sql = "INSERT INTO comment_like (user_id, comment_id) VALUES (?, ?)";

        try (PreparedStatement statement = connection.prepareStatement(sql);) {
            statement.setInt(1, userId);
            statement.setInt(2, commentId);
            count = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count == 1;
    }
}
