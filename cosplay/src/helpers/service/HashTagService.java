package helpers.service;

import database.dao.DaoFactory;
import database.dao.PersistException;
import database.sql.MyDaoFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


public class HashTagService {
    private Connection connection;

    private static volatile HashTagService hts;

    public static HashTagService getInstance() {
        HashTagService localInstance = hts;
        if (localInstance == null) {
            synchronized (HashTagService.class) {
                localInstance = hts;
                if (localInstance == null) {
                    hts = localInstance = new HashTagService();
                }
            }
        }
        return localInstance;
    }



    private HashTagService() {
        try {
            DaoFactory<Connection> factory = new MyDaoFactory();
            connection = factory.getContext();
        } catch (PersistException e) {
            e.printStackTrace();
        }
    }
    public List<String> pullHashtag(int cosplay_id) {
        List<String> list = new ArrayList<>();
        String sql = "SELECT name FROM hashtag JOIN hashtag_cosplay ON hashtag.id = hashtag_cosplay.hashtag_id WHERE cosplay_id = ?;";
        try (PreparedStatement statement = connection.prepareStatement(sql);) {
            statement.setInt(1, cosplay_id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                list.add(rs.getString("name"));
            }
        } catch (Exception e) {
            try {
                throw new PersistException(e);
            } catch (PersistException e1) {
                e1.printStackTrace();
            }
        }
        return list;
    }
}
