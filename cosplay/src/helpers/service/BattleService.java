package helpers.service;

import database.dao.DaoFactory;
import database.dao.GenericDao;
import database.dao.PersistException;

import database.models.Battle;
import database.models.Cosplay;
import database.sql.BattleDao;
import database.sql.MyDaoFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class BattleService {
    private GenericDao battleDao;
    private Connection connection;
    private static volatile BattleService bs;

    public static BattleService getInstance() {
        BattleService localInstance = bs;
        if (localInstance == null) {
            synchronized (BattleService.class) {
                localInstance = bs;
                if (localInstance == null) {
                    bs = localInstance = new BattleService();
                }
            }
        }
        return localInstance;
    }
    public GenericDao getBattleDao() {
        return battleDao;
    }

    private BattleService() {
        try {
            DaoFactory<Connection> factory = new MyDaoFactory();
            connection = factory.getContext();
            this.battleDao = factory.getDao(connection, Battle.class);
        } catch (PersistException e) {
            e.printStackTrace();
        }
    }
    protected List<Battle> parseResultSet(ResultSet rs) throws PersistException {
        CosplayService cosplayService = CosplayService.getInstance();
        List<Battle> result = new ArrayList<>();
        try {
            while (rs.next()) {
                BattleDao.PersistBattle battle = new BattleDao.PersistBattle();
                battle.setId(rs.getInt("id"));
                battle.setLeftCosplayId(rs.getInt("left_cosplay_id"));
                battle.setRightCosplayId(rs.getInt("right_cosplay_id"));
                battle.setLeftCosplay((Cosplay) cosplayService.getCosplayDao().getByPK(battle.getLeftCosplayId()));
                battle.setRightCosplay((Cosplay) cosplayService.getCosplayDao().getByPK(battle.getRightCosplayId()));
                battle.setBlockId(rs.getInt("comment_block_id"));
                battle.setHeader(rs.getString("header"));
                battle.setVotesSum(rs.getInt("votes_sum"));
                battle.setVotesCount(rs.getInt("votes_counter"));

                result.add(battle);
            }
        } catch (Exception e) {
            throw new PersistException(e);
        }
        return result;
    }

    public List<Battle> pullBattle(int offset, int limit, String querry, String param) {
        List<Battle> list = new ArrayList<>();
        String sql = "SELECT * FROM battle ORDER BY rating DESC, date DESC, TIME DESC OFFSET ? LIMIT ?;";
        int i = 1;
        if (querry != null)
            sql = querry;
        try (PreparedStatement statement = connection.prepareStatement(sql);) {
            if (querry != null)
                statement.setString(i++, param);
            statement.setInt(i++, offset);
            statement.setInt(i++, limit);
            ResultSet rs = statement.executeQuery();
            list = parseResultSet(rs);
        } catch (Exception e) {
            try {
                throw new PersistException(e);
            } catch (PersistException e1) {
                e1.printStackTrace();
            }
        }
        return list;

    }

    public int getMax() {
        String sql = "SELECT max(votes_counter) FROM battle;";
        try (PreparedStatement statement = connection.prepareStatement(sql);) {
            ResultSet rs = statement.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
