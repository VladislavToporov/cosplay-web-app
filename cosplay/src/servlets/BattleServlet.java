package servlets;

import database.dao.PersistException;
import database.models.Battle;
import database.models.Comment;
import database.models.Cosplay;
import database.models.User;
import helpers.service.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Random;

public class BattleServlet extends HttpServlet {
    CosplayService cosplayService;
    BattleService battleService;
    UserService userService;
    CommentService commentService;
    HashTagService hashTagService;
    public void init() throws ServletException {
        cosplayService =CosplayService.getInstance();
        battleService = BattleService.getInstance();
        userService = UserService.getInstance();
        commentService = CommentService.getInstance();
        hashTagService = HashTagService.getInstance();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserService userService = UserService.getInstance();
        int user_id = (int) request.getSession().getAttribute("id");
        List<Cosplay> cosplayList = userService.pullCosplay(user_id, 0, 10);
        Random random = new Random();
        int left = random.nextInt(cosplayList.size());
        int right = Integer.parseInt(request.getParameter("cosplay_id"));
        String rightName = request.getParameter("cosplay_name");

        Battle battle = new Battle();
        battle.setHeader(cosplayList.get(left).getHeader() + "vs" + rightName);
        battle.setLeftCosplayId(cosplayList.get(left).getId());
        battle.setRightCosplayId(right);
        try {
            battle = (Battle) battleService.getBattleDao().persist(battle);
        } catch (PersistException e) {
            e.printStackTrace();
        }
        response.sendRedirect("/battles?id=" + battle.getId());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer userId = (Integer)request.getSession().getAttribute("id");
        String bat = request.getParameter("id");

        if (bat == null) {
            List<Battle> list = battleService.pullBattle(0, 10, null, null);
            request.setAttribute("battle_list", list);
            request.getRequestDispatcher("/listofbattle.ftl").forward(request, response);



        } else {
            int battleId = Integer.parseInt(bat);
            try {
                Battle battle = (Battle) battleService.getBattleDao().getByPK(battleId);

                if (userId != null) {
                    User u = (User) userService.getUserDao().getByPK(userId);
                    request.setAttribute("user_id", userId);
                    request.setAttribute("user", u.getUsername());
                    request.setAttribute("battle_id", battleId);
                    request.setAttribute("block_id", battle.getBlockId());
                }

                request.setAttribute("battle", battle);

                List<Comment> listComment = commentService.pullComment("battle_block_id", battle.getBlockId(), 0, 10);
                request.setAttribute("comment_list", listComment);
            } catch (PersistException e) {
                e.printStackTrace();
            }
            request.getRequestDispatcher("/battle.ftl").forward(request, response);
        }

    }
}
