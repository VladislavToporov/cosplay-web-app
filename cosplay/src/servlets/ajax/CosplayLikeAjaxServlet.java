package servlets.ajax;

import database.dao.PersistException;
import database.models.Cosplay;
import helpers.service.CosplayService;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CosplayLikeAjaxServlet extends HttpServlet {
    CosplayService cosplayService;
    @Override
    public void init() throws ServletException {
        cosplayService = CosplayService.getInstance();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int userId = Integer.parseInt(request.getParameter("user_id"));
        int cosplayId = Integer.parseInt(request.getParameter("cosplay_id"));
        int mark = Integer.parseInt(request.getParameter("mark"));
        mark = mark > 10 ? 10 : mark;
        mark = mark < 0 ? 0 : mark;

        JSONObject jsonObject = new JSONObject();
        if (cosplayService.checkLike(userId, cosplayId)) {
            try {
                Cosplay cosplay = (Cosplay) cosplayService.getCosplayDao().getByPK(cosplayId);
                cosplay.setVotesSum(cosplay.getVotesSum() + mark);
                cosplay.setVotesCount(cosplay.getVotesCount() + 1);
                cosplay.calcRating();
                cosplayService.getCosplayDao().update(cosplay);
                jsonObject.put("data", "OK");
                jsonObject.put("rating", cosplay.getRating());
                jsonObject.put("counter", cosplay.getVotesCount());
            } catch (PersistException e) {
                e.printStackTrace();
            }
        }
        else jsonObject.put("data", "");


        response.setContentType("text/json");
        response.setCharacterEncoding("utf-8");
        response.getWriter().print(jsonObject.toString());
        System.out.println(jsonObject.toString());
        response.getWriter().close();

    }

}
