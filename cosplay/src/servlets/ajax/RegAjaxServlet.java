package servlets.ajax;

import helpers.service.UserService;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegAjaxServlet extends HttpServlet {
    UserService userService;

    public void init() throws ServletException {
        userService = UserService.getInstance();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("name_user");
        String email = request.getParameter("email_user");
        String password = request.getParameter("password_user");
        String password_2 = request.getParameter("password_2_user");

        Map<String, String> errorContainer = new HashMap<>();
        if (userService.checkKey("login", login))
            errorContainer.put("name_user", "Этот логин уже зарегистрирован");
        if (userService.checkKey("email", email))
            errorContainer.put("email_user", "Этот email уже зарегистрирован");
        if (!checkWithRegExp(password))
            errorContainer.put("password_user", "Пароль должен содержать более 6 цифр и латинских букв в разном регистре ");
        if (!password.equals(password_2))
            errorContainer.put("password_2_user", "Пароли не совпадают");
        JSONObject jo = new JSONObject();

        try {
            if (errorContainer.size() != 0) {
                jo.put("result", "error");
                jo.put("text_error", errorContainer);
            } else
                jo.put("result", "success");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        response.setContentType("text/json");
        response.setCharacterEncoding("utf-8");
        response.getWriter().print(jo.toString());
        System.out.println(jo.toString());
        response.getWriter().close();
    }

    public static boolean checkWithRegExp(String password) {
        //String[] reg = {"(?=.*[0-9])", "(?=.*[a-z])(?=.*[A-Z])", "[0-9a-zA-Z]{6,}"};
        String regex = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,}";


        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(password);
        return m.matches();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}

