package servlets.ajax;

import database.models.Cosplay;
import helpers.service.CosplayService;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class CosplayAjaxServlet extends HttpServlet {
    CosplayService cosplayService = CosplayService.getInstance();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int offset = Integer.parseInt(request.getParameter("offset"));
        System.out.println(offset);
        List<Cosplay> list = cosplayService.pullCosplay(offset, 2, null, null);

        JSONObject jsonObject = new JSONObject();
        JSONArray array = new JSONArray();
        for (Cosplay c : list) {
            JSONObject jo = new JSONObject(c);
            array.put(jo);
        }
        jsonObject.put("data", array);
        jsonObject.put("offset", offset += list.size());

        response.setContentType("text/json");
        response.setCharacterEncoding("utf-8");
        response.getWriter().print(jsonObject.toString());
        System.out.println(jsonObject.toString() + "\n");
        response.getWriter().close();
    }
}
