package servlets.ajax;

import database.dao.PersistException;
import database.models.Comment;
import helpers.service.CommentService;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class CommentAjaxServlet extends HttpServlet {
    private CommentService commentService;

    @Override
    public void init() throws ServletException {
        commentService = CommentService.getInstance();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String c = request.getParameter("comment");
        String isBattle = request.getParameter("is_battle");

        c = c.replace("<", "&lt;").replace(">", "&gt;")
                .replace("&", "&amp;");

        int user_id = Integer.parseInt(request.getParameter("user_id"));
        int block_id = Integer.parseInt(request.getParameter("block_id"));

        Comment comment = new Comment();
        comment.setContent(c);
        if (isBattle == null)
            comment.setBlockId(block_id);
        else
            comment.setBattleBlockId(block_id);
        comment.setUserId(user_id);

        JSONArray array = new JSONArray();
        try {
            commentService.getCommentDao().persist(comment);
            List<Comment> listComment;
            if (isBattle == null)
                listComment = commentService.pullComment("block_id", block_id, 0, 100);
            else
                listComment = commentService.pullComment("battle_block_id", block_id, 0, 100);

            for (Comment elem: listComment) {
                JSONObject jo = new JSONObject(elem);
                System.out.println(jo.toString());
                array.put(jo);
            }
        } catch (PersistException e) {
            e.printStackTrace();
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("data", array);

        response.setContentType("text/json");
        response.setCharacterEncoding("utf-8");
        response.getWriter().print(jsonObject.toString());
        System.out.println(jsonObject.toString());
        response.getWriter().close();
    }

    public static void main(String[] args) {
        String c = "comment";
        String isBattle = "is_battle";

        c = c.replace("<", "&lt;").replace(">", "&gt;")
                .replace("&", "&amp;");

        int user_id = 1;
        int block_id = 2;

        Comment comment = new Comment();
        comment.setContent(c);
        if (isBattle == null)
            comment.setBlockId(block_id);
        else
            comment.setBattleBlockId(block_id);
        comment.setUserId(user_id);

        CommentService commentService = CommentService.getInstance();
        try {
            commentService.getCommentDao().persist(comment);
            List<Comment> listComment;
            if (isBattle == null)
                listComment = commentService.pullComment("block_id", block_id, 0, 100);
            else
                listComment = commentService.pullComment("battle_block_id", block_id, 0, 100);

            System.out.println(listComment);
        } catch (PersistException e) {
            e.printStackTrace();
        }
    }
}
