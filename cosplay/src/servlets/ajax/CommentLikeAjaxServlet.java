package servlets.ajax;

import database.dao.PersistException;
import database.models.Comment;
import helpers.service.CommentService;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CommentLikeAjaxServlet extends HttpServlet {
    CommentService commentService;
    @Override
    public void init() throws ServletException {
        commentService = CommentService.getInstance();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int userId = Integer.parseInt(request.getParameter("user_id"));
        int commentId = Integer.parseInt(request.getParameter("comment_id"));
        JSONObject jsonObject = new JSONObject();

        if (commentService.checkLike(userId, commentId)) {
            try {
                Comment comment = (Comment) commentService.getCommentDao().getByPK(commentId);
                comment.setVotesSum(comment.getVotesSum() + 1);
                comment.calcRating();
                commentService.getCommentDao().update(comment);
                jsonObject.put("fail", false);
                jsonObject.put("rating", comment.getRating());
            } catch (PersistException e) {
                e.printStackTrace();
            }
        }
        else jsonObject.put("fail", true);


        response.setContentType("text/json");
        response.setCharacterEncoding("utf-8");
        response.getWriter().print(jsonObject.toString());
        response.getWriter().close();
    }
}
