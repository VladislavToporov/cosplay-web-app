package servlets;

import database.models.Cosplay;
import helpers.service.CosplayService;
import helpers.service.UserService;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.List;

public class MainServlet extends javax.servlet.http.HttpServlet {
    private UserService userService;
    private CosplayService cosplayService;

    @Override
    public void init() throws ServletException {
        userService = UserService.getInstance();
        cosplayService = CosplayService.getInstance();
    }

    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        userService.logout(request, response);
        response.sendRedirect("/login");

    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        /*
        Connection conn = DBConnectionSingleton.getInstance();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM students ORDER BY id");
            List<String> students = new ArrayList<>();
            while (rs.next()) {
                students.add(rs.getString("name"));
            }
            request.setAttribute("students", students);
             } catch (SQLException e) {
            e.printStackTrace();
        }
            */
        List<Cosplay> list = cosplayService.pullCosplay(0, 2, null, null);
        request.setAttribute("cosplay_list", list);
        request.setAttribute("offset", list.size());
        request.getRequestDispatcher("/index.ftl").forward(request, response);
    }

}
