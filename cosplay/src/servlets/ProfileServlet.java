package servlets;

import database.dao.PersistException;
import database.models.Cosplay;
import database.models.User;
import helpers.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ProfileServlet extends HttpServlet {
    UserService userService;
    @Override
    public void init() throws ServletException {
       userService = UserService.getInstance();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer userId = (Integer)request.getSession().getAttribute("id");
        String u = request.getParameter("id");

        if (u == null) {
            List<User> list = null;
            try {
                list = userService.getUserDao().getAll();
            } catch (PersistException e) {
                e.printStackTrace();
            }
            request.setAttribute("list_profiles", list);

            request.getRequestDispatcher("/listofprofiles.ftl").forward(request, response);



        } else {
            int uID = Integer.parseInt(u);
            try {
                User user = (User) userService.getUserDao().getByPK(uID);

                List<Cosplay> cosplayList = userService.pullCosplay(user.getId(), 0, 10);
                request.setAttribute("cosplay_list", cosplayList);
                request.setAttribute("user", user);

            } catch (PersistException e) {
                e.printStackTrace();
            }
            request.getRequestDispatcher("/profile.ftl").forward(request, response);
        }
    }
}
