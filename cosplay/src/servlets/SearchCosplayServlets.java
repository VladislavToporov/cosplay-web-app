package servlets;

import database.dao.PersistException;
import database.models.Cosplay;
import helpers.service.CosplayService;
import helpers.service.HashTagService;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SearchCosplayServlets extends HttpServlet {
    CosplayService cosplayService;
    HashTagService hashTagService;

    @Override
    public void init() throws ServletException {
        cosplayService = CosplayService.getInstance();
        hashTagService = HashTagService.getInstance();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String query = request.getParameter("query");
        String radio = request.getParameter("radio");


        List<Cosplay> list = new ArrayList<>();
        System.out.println("header");
        if (radio.equalsIgnoreCase("headerRadio")) {
            String sql = "SELECT * FROM cosplay WHERE header ~* ? ORDER BY rating DESC, date DESC, TIME DESC OFFSET ? LIMIT ?;";
            list = cosplayService.pullCosplay(0, 10, sql, query);
        } else if (radio.equalsIgnoreCase("tagRadio")) {
            System.out.println("tag");
            String[] items = query.toLowerCase().replace("#", "").split("[,._;!? ]*\\s+");
            try {
                list = cosplayService.getCosplayDao().getAll();
                list = list.stream().filter(cosplay -> cosplay.getTags().containsAll(Arrays.asList(items))).collect(Collectors.toList());
            } catch (PersistException e) {
                e.printStackTrace();
            }
        }

        JSONObject jsonObject = new JSONObject();
        JSONArray array = new JSONArray();
        for (Cosplay c : list) {
            JSONObject jo = new JSONObject(c);
            array.put(jo);
        }
        jsonObject.put("data", array);

        response.setContentType("text/json");
        response.setCharacterEncoding("utf-8");
        response.getWriter().print(jsonObject.toString());
        System.out.println(jsonObject.toString() + "\n");
        response.getWriter().close();
    }
}
