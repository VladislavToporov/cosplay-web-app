package servlets;

import helpers.service.UserService;
import helpers.MD5Util;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

public class LoginServlet extends HttpServlet {
    private UserService userService;

    @Override
    public void init() throws ServletException {
        userService = UserService.getInstance();
    }

    private int check(String username, String password) {
        /*
        Connection conn = DBConnectionSingleton.getInstance();
        try {
            PreparedStatement st = conn.prepareStatement(
                    "SELECT EXISTS(SELECT user_profile.login\n" +
                            "              FROM user_profile\n" +
                            "              WHERE user_profile.login = ? AND user_profile.password = ?);"
            );
            st.setString(1, username);
            st.setString(2, password);
            ResultSet rs = st.executeQuery();
            rs.next();
            return rs.getBoolean("exists");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
        */
        return userService.checkPL(username, password, "password");
        //return false;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String current_user = (String) request.getSession().getAttribute("current_user");
        if (current_user != null)
            response.sendRedirect("/index");
        else {
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            String password_hash = MD5Util.md5Custom(password);
            String remember = request.getParameter("remember_me");
            String token = MD5Util.md5Custom(MD5Util.salt());

            int id = check(username, password_hash);
            if (id > 0) {
                request.getSession().setAttribute("current_user", username);
                request.getSession().setAttribute("id", id);

                if (remember != null && remember.equals("1")) {
                    userService.updateToken(username, token);
                    /*
                    Connection conn = DBConnectionSingleton.getInstance();
                    try {
                        PreparedStatement ps = conn.prepareStatement("UPDATE user_profile SET token = ? WHERE login = ? ");
                        ps.setString(1, token);
                        ps.setString(2, username);
                        ps.executeUpdate();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    */
                    Cookie c1 = new Cookie("login", username);
                    c1.setMaxAge(60 * 60);
                    Cookie c2 = new Cookie("token", token);
                    c2.setMaxAge(60 * 60);
                    response.addCookie(c1);
                    response.addCookie(c2);
                }
                response.sendRedirect("/index");
            } else {
                request.setAttribute("flag", false);
                request.setAttribute("fail", true);
                request.getRequestDispatcher("/login.ftl").forward(request, response);
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String current_user = (String) request.getSession().getAttribute("current_user");
        if (current_user != null)
            response.sendRedirect("/index");
        else {
            request.setAttribute("flag", false);
            request.setAttribute("fail", false);
            request.getRequestDispatcher("/login.ftl").forward(request, response);

        }

    }
}
