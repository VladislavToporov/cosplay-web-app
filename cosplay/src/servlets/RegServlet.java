package servlets;

import database.dao.PersistException;
import database.models.User;
import helpers.service.UserService;
import helpers.DBConnectionSingleton;
import helpers.MD5Util;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;

public class RegServlet extends HttpServlet {
    UserService userService;
    @Override
    public void init() throws ServletException {
        userService = UserService.getInstance();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Connection conn = DBConnectionSingleton.getInstance();
        String username = request.getParameter("username");
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String password_hash = MD5Util.md5Custom(password);
        String email = request.getParameter("email");
        String token = MD5Util.md5Custom(MD5Util.salt());
        User user = new User(username, login, password_hash, email, token);
        try {
            userService.getUserDao().persist(user);
            } catch (PersistException e) {
            e.printStackTrace();
            }

        response.sendRedirect("/login");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/checkin.ftl").forward(request, response);
    }
}
