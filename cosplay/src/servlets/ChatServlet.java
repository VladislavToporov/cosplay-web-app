package servlets;

import database.dao.PersistException;
import database.models.Chat;
import database.models.Cosplay;
import database.models.Message;
import database.models.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ChatServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer userId = request.getSession().getAttribute("id") != null ?
                (Integer) request.getSession().getAttribute("id") : 0;
        String id = request.getParameter("id");
        if (userId == 0) {
            response.sendRedirect("/login");
        }
        if (id == null) {
            //List<Chat> list = chatService.pullChat(0, 10, null, null);
            //request.setAttribute("message_list", list);
            request.getRequestDispatcher("/direct.ftl").forward(request, response);

        } else {
            int chatId = Integer.parseInt(id);
            /*
            try {
                Chat chat = (Chat) chatService.getChatDao().getByPK(chatId);
                List<Message> list = messageService.pullMessage();

                User u = (User) userService.getUserDao().getByPK(userId);
                request.setAttribute("user_id", userId);
                request.setAttribute("user", u.getUsername());
                request.setAttribute("cosplay_id", chatId);
                request.setAttribute("block_id", chat.getBlockId());

                request.setAttribute("messages", list);

            } catch (PersistException e) {
                e.printStackTrace();
            }
            */
            request.getRequestDispatcher("/chat.ftl").forward(request, response);
        }
    }
}
