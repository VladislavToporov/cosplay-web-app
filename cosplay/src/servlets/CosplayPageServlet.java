package servlets;

import database.dao.PersistException;
import database.models.Comment;
import database.models.Cosplay;
import database.models.User;
import helpers.service.CommentService;
import helpers.service.CosplayService;
import helpers.service.HashTagService;
import helpers.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class CosplayPageServlet extends HttpServlet {
    CosplayService cosplayService;
    UserService userService;
    CommentService commentService;
    HashTagService hashTagService;
    public void init() throws ServletException {
        cosplayService = CosplayService.getInstance();
        userService = UserService.getInstance();
        commentService = CommentService.getInstance();
        hashTagService = HashTagService.getInstance();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String header = request.getParameter("header");
        String description = request.getParameter("description");
        int userId = Integer.parseInt(request.getParameter("user_id"));

        Cosplay cosplay = new Cosplay();
        cosplay.setHeader(header);
        cosplay.setDescription(description);
        cosplay.setUserId(userId);
        try {
            cosplayService.getCosplayDao().persist(cosplay);
        } catch (PersistException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer userId = (Integer)request.getSession().getAttribute("id");
        String cos = request.getParameter("id");

        if (cos == null) {
            List<Cosplay> list = cosplayService.pullCosplay(0, 10, null, null);
            request.setAttribute("cosplay_list", list);
            request.getRequestDispatcher("/listofcosplay.ftl").forward(request, response);



        } else {
            int cosplayId = Integer.parseInt(cos);
            try {
                Cosplay cosplay = (Cosplay) cosplayService.getCosplayDao().getByPK(cosplayId);

                if (userId != null) {
                    User u = (User) userService.getUserDao().getByPK(userId);
                    request.setAttribute("user_id", userId);
                    request.setAttribute("user", u.getUsername());
                    request.setAttribute("cosplay_id", cosplayId);
                    request.setAttribute("block_id", cosplay.getBlockId());
                }

                request.setAttribute("cosplay", cosplay);

                List<String> tags = hashTagService.pullHashtag(cosplayId);
                request.setAttribute("tags", tags);
                List<Comment> listComment = commentService.pullComment("block_id", cosplay.getBlockId(), 0, 10);
                request.setAttribute("comment_list", listComment);
            } catch (PersistException e) {
                e.printStackTrace();
            }
            request.getRequestDispatcher("/cosplaypage.ftl").forward(request, response);
        }

    }
}
