package servlets;

import database.dao.PersistException;
import database.models.Cosplay;
import helpers.service.CosplayService;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.util.Random;

@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 10,      // 10MB
        maxRequestSize = 1024 * 1024 * 50)   // 50MB

public class CosplayAddServlet extends HttpServlet {
    CosplayService cosplayService;

    @Override
    public void init() throws ServletException {
        cosplayService = CosplayService.getInstance();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        if (request.getSession().getAttribute("current_user") == null)
            response.sendRedirect("/index");
        String header = request.getParameter("header");
        String description = request.getParameter("description");
        int userId = (int)request.getSession().getAttribute("id");

        Cosplay cosplay = new Cosplay();
        cosplay.setHeader(header);
        cosplay.setDescription(description);
        cosplay.setUserId(userId);

        String savePath = getServletConfig().getServletContext().getRealPath("upload");
        File fileSaveDir = new File(savePath);
        if (!fileSaveDir.exists()) {
            fileSaveDir.mkdir();
        }

        //List<String> listFilename = new ArrayList<>();
        for (Part part : request.getParts()) {
            Random random = new Random();
            String filePath = extractFileName(part);

            if (filePath.length() > 2) {
                String ext = filePath.substring(filePath.lastIndexOf("."));
                File uploadetFile;
                String fileName;
                do {
                    fileName = random.nextInt() + ext;
                    filePath = savePath + File.separator + fileName;
                    uploadetFile = new File(filePath);
                } while (uploadetFile.exists());
                part.write(filePath);
                //listFilename.add(fileName);
                cosplay.setImage(fileName);
            }

        }

        try {
            cosplayService.getCosplayDao().persist(cosplay);
        } catch (PersistException e) {
            e.printStackTrace();
        }
        //request.setAttribute("message", "Upload has been done successfully!");

        response.sendRedirect("/index");
    }

    private String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length() - 1);
            }
        }
        return "";
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/addcosplay.ftl").forward(request, response);
    }
}
