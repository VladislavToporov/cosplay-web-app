package filters;

import helpers.service.UserService;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Objects;

public class AuthFilter implements Filter {
    public void destroy() {
    }

    public int check(String login, String token) {
        /*
        Connection conn = DBConnectionSingleton.getInstance();
        try {
            PreparedStatement st = conn.prepareStatement(
                    "SELECT EXISTS(SELECT user_profile.login" +
                            "              FROM user_profile" +
                            "              WHERE user_profile.login = ? AND user_profile.token = ?);"
            );
            st.setString(1, login);
            st.setString(2, token);
            ResultSet rs = st.executeQuery();
            rs.next();
            return rs.getBoolean("exists");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
        */
        return userService.checkPL(login, token, "token");
    }


    private String[] findCookie(Cookie[] cookies) {
        boolean loginFlag = false;
        boolean tokenFlag = false;
        String[] elem = {"", "", "false"};
        for (Cookie c : cookies) {
            if (Objects.equals(c.getName(), "login") && c.getMaxAge() == 60 * 60) {
                loginFlag = true;
                elem[0] = c.getValue();
            }
            if (Objects.equals(c.getName(), "token") && c.getMaxAge() == 60 * 60) {
                tokenFlag = true;
                elem[1] = c.getValue();
            }
            if (tokenFlag && loginFlag) {
                elem[2] = "true";
                return elem;
            }
        }
        return elem;
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        Cookie[] cookies = request.getCookies();

        String[] result = findCookie(cookies);
        if (Boolean.parseBoolean(result[2])) {
            if (request.getSession().getAttribute("current_user") == null) {
                String login = result[0];
                String token = result[1];

                int id = check(login, token);
                if (id > 0) {
                    request.getSession().setAttribute("current_user", login);
                    request.getSession().setAttribute("id", id);
                }
            /*
            else {
            ((HttpServletResponse) resp).sendRedirect("/login");
			}
			*/
            }
        }

        boolean flag = false;
        if (request.getSession().getAttribute("current_user") != null)
            flag = true;
        request.setAttribute("flag", flag);

        /*
        if (request.getSession().getAttribute("flag") != null &&
                (Boolean) request.getSession().getAttribute("flag")) {
            request.setAttribute("flag", false);
            request.getSession().setAttribute("flag", true);
        }
        */
        chain.doFilter(req, resp);
        /*
        else {
            ((HttpServletResponse) resp).sendRedirect("/login");
        }
        */
    }

    private UserService userService;

    public void init(FilterConfig config) throws ServletException {
        userService = UserService.getInstance();
    }


}
