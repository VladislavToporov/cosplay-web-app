package database;

import database.dao.*;
import database.models.*;
import database.sql.MyDaoFactory;
import database.sql.UserDao;
import helpers.MD5Util;

import java.sql.Connection;
import java.util.List;

public class Main {
    public static void main(String[] args) throws PersistException {
        DaoFactory<Connection> factory = new MyDaoFactory();
        Connection connection = factory.getContext();
        //GenericDao dao1 = factory.getDao(connection, Group.class);
        //GenericDao dao2 = factory.getDao(connection, Student.class);
        GenericDao<User, Integer> dao3 = factory.getDao(connection, User.class);
        //System.out.println(dao1.getAll());
        //System.out.println(dao2.getByPK(7));
        //Student s = new Student(4, "ToporovVlad", 7);
        //dao2.persist(s);

        /*
        User user = new User("nameI", "nameI", "passwordInsert", "emailI", "token");
        User user2 = dao3.persist(user);
        System.out.println(user2);
        dao3.getByPK(4);
        GenericDao<Cosplay, Integer> dao4 = factory.getDao(connection, Cosplay.class);
        dao4.getByPK(3);
        */

        GenericDao<Comment, Integer> dao4 = factory.getDao(connection, Comment.class);
        Comment comment = new Comment();
        comment.setBlockId(1);
        comment.setUserId(1);
        comment.setContent("lorem");
        dao4.persist(comment);
        /*
        User user = new User();
        user.setId(1);
        user.setLogin("vlad");
        user.setUsername("vlad");
        user.setPassword(MD5Util.md5Custom("vlad"));
        user.setEmail("vlad");
        dao3.persist(user);
        */

    }
}
