package database.dao;

/** Фабрика объектов для работы с базой данных */
public interface DaoFactory<Context> {

    interface DaoCreator<Context> {
        GenericDao create(Context context);
    }
    Context getContext() throws PersistException;
    GenericDao getDao(Context context, Class dtoClass) throws PersistException;
}
