package database.dao;

import java.io.Serializable;
import java.util.List;

/**
 * Унифицированный интерфейс управления персистентным состоянием объектов
 *
 * @param <T>  тип объекта персистенции
 * @param <PK> тип первичного ключа
 */
public interface GenericDao<T extends Identified<PK>, PK extends Serializable> {

    T create() throws PersistException;

    T persist(T object) throws PersistException;

    T getByPK(PK key) throws PersistException;

    T update(T object) throws PersistException;

    void delete(T object) throws PersistException;

    List<T> getAll() throws PersistException;

    //boolean getUserByPL(Connection connection, String login, String password, String type) throws PersistException;
}
