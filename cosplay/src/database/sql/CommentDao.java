package database.sql;

import database.dao.AbstractJDBCDao;
import database.dao.PersistException;
import database.models.Comment;
import database.models.User;
import helpers.service.UserService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CommentDao extends AbstractJDBCDao<Comment, Integer> {

    public static class PersistComment extends Comment {
        public void setId(int id) {
            super.setId(id);
        }
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM comment ";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO comment (user_id, block_id, content, battle_block_id) " +
                "VALUES (?,?,?,?) " +
                "RETURNING *;";
    }

    @Override
    public String getUpdateQuery() {

        return "UPDATE comment \n" +
                "SET user_id = ?, comment_block_id = ?, \n " +
                "album_id = ?, content = ?, votes_sum = ?, votes_counter = ?, rating = ?, battle_block_id \n" +
                "WHERE id = ? \n" +
                "RETURNING *;";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM comment WHERE id= ?;";
    }

    @Override
    public Comment create() throws PersistException {
        Comment s = new Comment();
        return persist(s);
    }

    public CommentDao(Connection connection) {
        super(connection);
    }

    @Override
    protected List<Comment> parseResultSet(ResultSet rs) throws PersistException {
        UserService userService = UserService.getInstance();
        List<Comment> result = new ArrayList<>();
        try {
            while (rs.next()) {
                PersistComment comment = new PersistComment();
                comment.setId(rs.getInt("id"));
                comment.setUserId(rs.getInt("user_id"));
                User user = (User) userService.getUserDao().getByPK(comment.getUserId());
                comment.setUsername(user.getUsername());
                comment.setBlockId(rs.getInt("block_id"));
                comment.setBattleBlockId(rs.getInt("battle_block_id"));
                comment.setAlbumId(rs.getInt("album_id"));
                comment.setContent(rs.getString("content"));
                comment.setVotesSum(rs.getInt("votes_sum"));
                comment.setVotesCount(rs.getInt("votes_counter"));
                comment.setRating(rs.getDouble("rating"));

                result.add(comment);
            }
        } catch (Exception e) {
            throw new PersistException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Comment object) throws PersistException {
        try {
            statement.setInt(1, object.getUserId());
            statement.setInt(2, object.getBlockId());
            statement.setInt(3, object.getAlbumId());
            statement.setString(4, object.getContent());
            statement.setInt(5, object.getVotesSum());
            statement.setInt(6, object.getVotesCount());
            statement.setDouble(7, object.getRating());
            statement.setDouble(8, object.getBattleBlockId());
            statement.setInt(9, object.getId());
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Comment object) throws PersistException {
        try {
            statement.setInt(1, object.getUserId());
            if (object.getBlockId() != null)
                statement.setInt(2, object.getBlockId());
            else
                statement.setObject(2, null);
            statement.setString(3, object.getContent());
            if (object.getBattleBlockId() != null)
                statement.setInt(4, object.getBattleBlockId());
            else
                statement.setObject(4, null);

        } catch (Exception e) {
            throw new PersistException(e);
        }
    }

}
