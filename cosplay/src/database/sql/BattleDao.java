package database.sql;

import database.dao.AbstractJDBCDao;
import database.dao.PersistException;
import database.models.Battle;
import database.models.Cosplay;
import helpers.service.CosplayService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class BattleDao extends AbstractJDBCDao<Battle, Integer> {

    public static  class PersistBattle extends Battle {
        public void setId(int id) {
            super.setId(id);
        }
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM battle ";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO battle " +
                "(left_cosplay_id, right_cosplay_id, header) " +
                "VALUES (?,?,?) " +
                "RETURNING *;";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE battle \n" +
                "SET left_cosplay_id = ?, right_cosplay_id = ?, \n " +
                "comment_block_id = ?, header = ?,  votes_sum = ?, votes_counter = ? \n" +
                "WHERE id = ? \n" +
                "RETURNING *;";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM battle WHERE id= ?;";
    }

    @Override
    public Battle create() throws PersistException {
        Battle s = new Battle();
        return persist(s);
    }

    public BattleDao(Connection connection) {
        super(connection);
    }

    @Override
    protected List<Battle> parseResultSet(ResultSet rs) throws PersistException {
        CosplayService cosplayService = CosplayService.getInstance();
        List<Battle> result = new ArrayList<>();
        try {
            while (rs.next()) {
                PersistBattle battle = new PersistBattle();
                battle.setId(rs.getInt("id"));
                battle.setLeftCosplayId(rs.getInt("left_cosplay_id"));
                battle.setRightCosplayId(rs.getInt("right_cosplay_id"));
                battle.setLeftCosplay((Cosplay) cosplayService.getCosplayDao().getByPK(battle.getLeftCosplayId()));
                battle.setRightCosplay((Cosplay) cosplayService.getCosplayDao().getByPK(battle.getRightCosplayId()));
                battle.setBlockId(rs.getInt("comment_block_id"));
                battle.setHeader(rs.getString("header"));
                battle.setVotesSum(rs.getInt("votes_sum"));
                battle.setVotesCount(rs.getInt("votes_counter"));
                result.add(battle);
            }
        } catch (Exception e) {
            throw new PersistException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Battle object) throws PersistException {
        try {
            statement.setInt(1, object.getLeftCosplayId());
            statement.setInt(2, object.getRightCosplayId());
            statement.setInt(3, object.getBlockId());
            statement.setString(4, object.getHeader());
            statement.setInt(5, object.getVotesSum());
            statement.setInt(6, object.getVotesCount());
            statement.setInt(7, object.getId());
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Battle object) throws PersistException {
        try {
            statement.setInt(1, object.getLeftCosplayId());
            statement.setInt(2, object.getRightCosplayId());
            statement.setString(3, object.getHeader());

        } catch (Exception e) {
            throw new PersistException(e);
        }
    }

}
