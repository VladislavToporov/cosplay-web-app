package database.sql;

import database.dao.AbstractJDBCDao;
import database.dao.PersistException;
import database.models.Chat;
import database.models.User;
import helpers.service.UserService;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ChatDao extends AbstractJDBCDao<Chat, Integer> {

    private class PersistChat extends Chat {
        public void setId(int id) {
            super.setId(id);
        }
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM chat ";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO chat (sender, recipient) VALUES (?, ?) " +
                "RETURNING *;";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE chat \n" +
                "SET sender, recipient = ? \n " +
                "WHERE id = ? \n" +
                "RETURNING *;";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM chat WHERE id= ?;";
    }

    @Override
    public Chat create() throws PersistException {
        Chat s = new Chat();
        return persist(s);
    }

    public ChatDao(Connection connection) {
        super(connection);
    }

    @Override
    protected List<Chat> parseResultSet(ResultSet rs) throws PersistException {
        UserService userService = UserService.getInstance();
        List<Chat> result = new ArrayList<>();
        try {
            while (rs.next()) {
                PersistChat chat = new PersistChat();
                chat.setId(rs.getInt("id"));
                chat.setSenderId(rs.getInt("sender"));
                chat.setRecipientId(rs.getInt("recipient"));
                chat.setSender((User)userService.getUserDao().getByPK(chat.getSenderId()));
                chat.setRecipient((User)userService.getUserDao().getByPK(chat.getRecipientId()));

                result.add(chat);
            }
        } catch (Exception e) {
            throw new PersistException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Chat object) throws PersistException {
        try {
            statement.setInt(1, object.getSenderId());
            statement.setInt(2, object.getRecipientId());
            statement.setInt(3, object.getId());
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Chat object) throws PersistException {
        try {
            statement.setInt(1, object.getSenderId());
            statement.setInt(2, object.getRecipientId());

        } catch (Exception e) {
            throw new PersistException(e);
        }
    }

}
