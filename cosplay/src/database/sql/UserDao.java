package database.sql;

import database.dao.AbstractJDBCDao;
import database.dao.PersistException;
import database.models.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class UserDao extends AbstractJDBCDao<User, Integer> {

    public static class PersistUser extends User {
        public void setId(int id) {
            super.setId(id);
        }
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM user_profile ";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO user_profile (username, login, password, email, token) \n" +
                "VALUES (?, ?, ?, ?, ?) \n" +
                "RETURNING *;";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE user_profile \n" +
                "SET username = ?, login = ?, \n " +
                "password = ?, email = ?,  token = ?, rating = ? \n" +
                "WHERE id = ? \n" +
                "RETURNING *;";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM user_profile WHERE id= ?;";
    }

    @Override
    public User create() throws PersistException {
        User s = new User();
        return persist(s);
    }

    public UserDao(Connection connection) {
        super(connection);
    }

    @Override
    protected List<User> parseResultSet(ResultSet rs) throws PersistException {
        List<User> result = new ArrayList<>();
        try {
            while (rs.next()) {
                PersistUser user = new PersistUser();
                user.setId(rs.getInt("id"));
                user.setUsername(rs.getString("username"));
                user.setLogin(rs.getString("login"));
                user.setPassword(rs.getString("password"));
                user.setEmail(rs.getString("email"));
                user.setToken(rs.getString("token"));
                user.setRating(rs.getDouble("rating"));
                user.setImage(rs.getString("image"));

                result.add(user);
            }
        } catch (Exception e) {
            throw new PersistException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, User object) throws PersistException {
        try {
            statement.setString(1, object.getUsername());
            statement.setString(2, object.getLogin());
            statement.setString(3, object.getPassword());
            statement.setString(4, object.getEmail());
            statement.setString(5, object.getToken());
            statement.setDouble(6, object.getRating());
            statement.setInt(7, object.getId());
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, User object) throws PersistException {
        try {
            statement.setString(1, object.getUsername());
            statement.setString(2, object.getLogin());
            statement.setString(3, object.getPassword());
            statement.setString(4, object.getEmail());
            statement.setString(5, object.getToken());

        } catch (Exception e) {
            throw new PersistException(e);
        }
    }

}
