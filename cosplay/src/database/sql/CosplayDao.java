package database.sql;

import database.dao.AbstractJDBCDao;
import database.dao.PersistException;
import database.models.Cosplay;
import database.models.User;
import helpers.service.HashTagService;
import helpers.service.UserService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CosplayDao extends AbstractJDBCDao<Cosplay, Integer> {
    public static class PersistCosplay extends Cosplay {
        public void setId(int id) {
            super.setId(id);
        }
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM cosplay ";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO cosplay (user_id, header, description, image) " +
                "VALUES (?,?,?,?) " +
                "RETURNING *;";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE cosplay \n" +
                "SET user_id = ?, comment_block_id = ?, \n " +
                "header = ?, description = ?, votes_sum = ?, votes_counter = ?, rating = ?, image = ? \n" +
                "WHERE id = ? \n" +
                "RETURNING *;";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM cosplay WHERE id= ?;";
    }

    @Override
    public Cosplay create() throws PersistException {
        Cosplay s = new Cosplay();
        return persist(s);
    }

    public CosplayDao(Connection connection) {
        super(connection);
    }

    @Override
    protected List<Cosplay> parseResultSet(ResultSet rs) throws PersistException {
        UserService userService = UserService.getInstance();
        HashTagService hashTagService = HashTagService.getInstance();
        List<Cosplay> result = new ArrayList<>();

        try {
            while (rs.next()) {
                PersistCosplay cosplay = new PersistCosplay();
                cosplay.setId(rs.getInt("id"));
                cosplay.setUserId(rs.getInt("user_id"));
                cosplay.setBlockId(rs.getInt("comment_block_id"));
                cosplay.setAlbumId(rs.getInt("album_id"));
                cosplay.setHeader(rs.getString("header"));
                cosplay.setDescription(rs.getString("description"));
                cosplay.setVotesSum(rs.getInt("votes_sum"));
                cosplay.setUser((User) userService.getUserDao().getByPK(cosplay.getUserId()));
                cosplay.setVotesCount(rs.getInt("votes_counter"));
                cosplay.setRating(rs.getDouble("rating"));
                cosplay.setImage(rs.getString("image"));
                List<String> tags = hashTagService.pullHashtag(cosplay.getId());
                cosplay.setTags(tags);
                result.add(cosplay);
            }
        } catch (Exception e) {
            throw new PersistException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Cosplay object) throws PersistException {
        try {
            statement.setInt(1, object.getUserId());
            statement.setInt(2, object.getBlockId());
            statement.setString(3, object.getHeader());
            statement.setString(4, object.getDescription());
            statement.setInt(5, object.getVotesSum());
            statement.setInt(6, object.getVotesCount());
            statement.setDouble(7, object.getRating());
            statement.setString(8, object.getImage());
            statement.setInt(9, object.getId());
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Cosplay object) throws PersistException {
        try {
            statement.setInt(1, object.getUserId());
            statement.setString(2, object.getHeader());
            statement.setString(3, object.getDescription());
            statement.setString(4, object.getImage());
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }

}
