package database.sql;

import database.dao.AbstractJDBCDao;
import database.dao.PersistException;
import database.models.Album;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class AlbumDao extends AbstractJDBCDao<Album, Integer> {

    private class PersistAlbum extends Album {
        public void setId(int id) {
            super.setId(id);
        }
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM album ";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO album (folder_path) VALUES (?) " +
                "RETURNING *;";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE album \n" +
                "SET folder_path = ? \n " +
                "WHERE id = ? \n" +
                "RETURNING *;";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM album WHERE id= ?;";
    }

    @Override
    public Album create() throws PersistException {
        Album s = new Album();
        return persist(s);
    }

    public AlbumDao(Connection connection) {
        super(connection);
    }

    @Override
    protected List<Album> parseResultSet(ResultSet rs) throws PersistException {

        List<Album> result = new ArrayList<>();
        try {
            while (rs.next()) {
                PersistAlbum album = new PersistAlbum();
                album.setId(rs.getInt("id"));
                album.setPath(rs.getString("folder_path"));

                result.add(album);
            }
        } catch (Exception e) {
            throw new PersistException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Album object) throws PersistException {
        try {
            statement.setString(1, object.getPath());
            statement.setInt(2, object.getId());
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Album object) throws PersistException {
        try {
            statement.setString(1, object.getPath());

        } catch (Exception e) {
            throw new PersistException(e);
        }
    }

}
