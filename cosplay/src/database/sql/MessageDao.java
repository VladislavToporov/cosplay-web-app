package database.sql;

import database.dao.AbstractJDBCDao;
import database.dao.PersistException;
import database.models.Message;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class MessageDao extends AbstractJDBCDao<Message, Integer> {

    private class PersistMessage extends Message {
        public void setId(int id) {
            super.setId(id);
        }
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM message ";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO message (chat_id, content) VALUES (?, ?) " +
                "RETURNING *;";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE message \n" +
                "SET chat_id = ?, content = ? \n " +
                "WHERE id = ? \n" +
                "RETURNING *;";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM message WHERE id= ?;";
    }

    @Override
    public Message create() throws PersistException {
        Message s = new Message();
        return persist(s);
    }

    public MessageDao(Connection connection) {
        super(connection);
    }

    @Override
    protected List<Message> parseResultSet(ResultSet rs) throws PersistException {

        List<Message> result = new ArrayList<>();
        try {
            while (rs.next()) {
                PersistMessage message = new PersistMessage();
                message.setId(rs.getInt("id"));
                message.setChatID(rs.getInt("chat_id"));
                message.setContent(rs.getString("content"));

                result.add(message);
            }
        } catch (Exception e) {
            throw new PersistException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Message object) throws PersistException {
        try {
            statement.setInt(1, object.getChatID());
            statement.setString(2, object.getContent());
            statement.setInt(3, object.getId());
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Message object) throws PersistException {
        try {
            statement.setInt(1, object.getChatID());
            statement.setString(2, object.getContent());

        } catch (Exception e) {
            throw new PersistException(e);
        }
    }

}
