package database.sql;

import database.dao.*;
import database.models.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class MyDaoFactory implements DaoFactory<Connection> {
    private Map<Class, DaoCreator> creators;

    public Connection getContext() throws PersistException {
        Connection connection = null;
        try {
            String driver = "org.postgresql.Driver";
            Class.forName(driver);//Регистрируем драйвер
            String user = "easy_cosplay_owner";
            String password = "easy_cosplay_owner";
            String url = "jdbc:postgresql://localhost:5432/easy_cosplay";
            connection = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            throw new PersistException(e);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return  connection;
    }

    @Override
    public GenericDao getDao(Connection connection, Class dtoClass) throws PersistException {
        DaoCreator creator = creators.get(dtoClass);
        if (creator == null) {
            throw new PersistException("Dao object for " + dtoClass + " not found.");
        }
        return creator.create(connection);
    }

    public MyDaoFactory() {
        creators = new HashMap<>();
        creators.put(User.class, (DaoCreator<Connection>) connection -> new UserDao(connection));
        creators.put(Cosplay.class, (DaoCreator<Connection>) connection -> new CosplayDao(connection));
        creators.put(Comment.class, (DaoCreator<Connection>) connection -> new CommentDao(connection));
        creators.put(Battle.class, (DaoCreator<Connection>) connection -> new BattleDao(connection));
    }
}
