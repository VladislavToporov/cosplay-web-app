package database.models;

import database.dao.Identified;
import helpers.service.CommentService;

public class Comment implements Identified<Integer> {
    CommentService commentService = CommentService.getInstance();
    private static int maxCountVotes = 0;
    private Integer id;
    private String content;
    private int userId;
    private int albumId;
    private Integer blockId;
    private int votesCount;
    private int votesSum;
    private User user;
    private String username;
    private Integer battleBlockId;
    private double rating;

    public Comment() {
        this.votesCount = 0;
        this.votesSum = 0;
        Comment.maxCountVotes = commentService.getMax();
        rating = calcRating();
    }



    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getAlbumId() {
        return albumId;
    }

    public void setAlbumId(int albumId) {
        this.albumId = albumId;
    }

    public Integer getBlockId() {
        return blockId;
    }

    public void setBlockId(int blockId) {
        this.blockId = blockId;
    }

    public int getVotesCount() {
        return votesCount;
    }

    public int getVotesSum() {
        return votesSum;
    }

    public void setVotesSum(int votesSum) {
        this.votesSum = votesSum;
    }

    public double getRating() {
        return rating;
    }



    public double calcRating() {
        if (votesCount == 0)
            return 0;
        double rating = (Math.log(this.votesCount) / Math.log(Math.pow(maxCountVotes, 0.1)));
        return Double.parseDouble(String.format("%(.2f Default locale", rating));
    }

    public void setVotesCount(int votesCount) {
        this.votesCount = votesCount;
        maxCountVotes = Math.max(maxCountVotes, votesCount);
    }

    public User getUser() {
        return user;
    }
    public String getUsername() {
        return user.getUsername();
    }

    public void addVotesSum(int mark) {
        this.votesSum += mark;
        this.votesCount += 1;
        maxCountVotes = Math.max(maxCountVotes, votesCount);
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public String toString() {
        return getContent();
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setBattleBlockId(int battleBlockId) {
        this.battleBlockId = battleBlockId;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public Integer getBattleBlockId() {
        return battleBlockId;
    }
}