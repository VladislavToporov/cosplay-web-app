package database.models;

import database.dao.Identified;

public class Block implements Identified<Integer> {
    private Integer id;

    public void setId(Integer id) {
        this.id = id;
    }

    public Block(Integer id) {
        this.id = id;
    }

    @Override
    public Integer getId() {
        return id;
    }
}