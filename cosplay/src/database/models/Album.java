package database.models;

import database.dao.Identified;

public class Album implements Identified<Integer> {
    private Integer id;
    private String path;

    public void setId(Integer id, String path) {
        this.id = id;
        this.path = path;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Album() {}

    @Override
    public Integer getId() {
        return id;
    }
}