package database.models;

import database.dao.Identified;

public class Chat implements Identified<Integer> {
    private Integer id;
    private int senderId;
    private int recipientId;
    private User sender;
    private User recipient;

    public User getSender() {
        return sender;
    }

    public void setSenderId(int sender) {
        this.senderId = sender;
    }

    public User getRecipient() {
        return recipient;
    }

    public void setRecipientId(int recipient) {
        this.recipientId = recipient;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Chat() {}

    @Override
    public Integer getId() {
        return id;
    }

    public int getSenderId() {
        return senderId;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public int getRecipientId() {
        return recipientId;
    }

    public void setRecipient(User recipient) {
        this.recipient = recipient;
    }
}