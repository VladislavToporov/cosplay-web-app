package database.models;

import database.dao.Identified;
import helpers.service.CosplayService;

import java.util.List;
import java.util.Locale;

public class Cosplay implements Identified<Integer> {
    CosplayService cosplayService = CosplayService.getInstance();
    private static int maxCountVotes = 0;
    private Integer id;
    private User user;
    private List<String> tags;
    private String image;


    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getAlbumId() {
        return albumId;
    }

    public void setAlbumId(int albumId) {
        this.albumId = albumId;
    }

    public int getBlockId() {
        return blockId;
    }

    public void setBlockId(int blockId) {
        this.blockId = blockId;
    }

    public int getVotesCount() {
        return votesCount;
    }

    public int getVotesSum() {
        return votesSum;
    }

    public void setVotesSum(int votesSum) {
        this.votesSum = votesSum;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    private String header;
    private String description;
    private int userId;
    private int albumId;
    private int blockId;
    private int votesCount;
    private int votesSum;
    private double rating;

    public Cosplay() {
        this.votesCount = 0;
        this.votesSum = 0;
        Cosplay.maxCountVotes = cosplayService.getMax();
        rating = calcRating();
    }

    public double calcRating() {
        if (votesCount == 0)
            return 0;
        double rating = (Math.log(this.votesCount) / Math.log(Math.pow(maxCountVotes, 0.1)) + votesSum / votesCount) / 2;
        if (rating > 10)
            rating = 10;
        return Double.parseDouble(String.format(Locale.ENGLISH, "%(.2f", rating));
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    @Override
    public String toString() {
        return "";
    }

    public void setVotesCount(int votesCount) {
        this.votesCount = votesCount;
        maxCountVotes = Math.max(maxCountVotes, votesCount);
    }

    public void addVotesSum(int mark) {
        this.votesSum += mark;
        this.votesCount += 1;
        maxCountVotes = Math.max(maxCountVotes, votesCount);
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }



}