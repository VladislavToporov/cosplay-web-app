package database.models;

import database.dao.Identified;
import helpers.service.BattleService;

public class Battle implements Identified<Integer> {
    BattleService battleService = BattleService.getInstance();
    private static int maxCountVotes = 0;
    private Integer id;
    private String header;
    private int leftCosplayId;
    private int rightCosplayId;
    private int blockId;
    private double leftRating;
    private double RightRating;
    private Cosplay leftCosplay;
    private Cosplay rightCosplay;
    private int userId;


    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public int getLeftCosplayId() {
        return leftCosplayId;
    }

    public void setLeftCosplayId(int leftCosplayId) {
        this.leftCosplayId = leftCosplayId;
    }

    public int getRightCosplayId() {
        return rightCosplayId;
    }

    public void setRightCosplayId(int rightCosplayId) {
        this.rightCosplayId = rightCosplayId;
    }

    public int getBlockId() {
        return blockId;
    }

    public void setBlockId(int blockId) {
        this.blockId = blockId;
    }

    public double getLeftRating() {
        return leftRating;
    }

    public void setLeftRating(double leftRating) {
        this.leftRating = leftRating;
    }

    public double getRightRating() {
        return RightRating;
    }

    public void setRightRating(double rightRating) {
        RightRating = rightRating;
    }

    public int getVotesCount() {
        return votesCount;
    }

    public int getVotesSum() {
        return votesSum;
    }

    public void setVotesSum(int votesSum) {
        this.votesSum = votesSum;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    private int votesCount;
    private int votesSum;
    private double rating;

    public Battle() {
        this.votesCount = 0;
        this.votesSum = 0;
        Battle.maxCountVotes = battleService.getMax();
        rating = calcRating();
    }

    public double calcRating() {
        if (votesCount == 0)
            return 0;
        double rating = (Math.log(this.votesCount) / Math.log(Math.pow(maxCountVotes, 0.1)) + votesSum / votesCount) / 2;
        return Double.parseDouble(String.format("%(.2f Default locale", rating));
    }

    public Cosplay getLeftCosplay() {
        return leftCosplay;
    }

    public Cosplay getRightCosplay() {
        return rightCosplay;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getId() {
        return id;
    }

    @Override
    public String toString() {
        return "";
    }

    public void setVotesCount(int votesCount) {
        this.votesCount = votesCount;
        maxCountVotes = Math.max(maxCountVotes, votesCount);
    }

    public void addVotesSum(int mark) {
        this.votesSum += mark;
        this.votesCount += 1;
        maxCountVotes = Math.max(maxCountVotes, votesCount);
    }

    public void setLeftCosplay(Cosplay cosplay) {
        this.leftCosplay = cosplay;
    }

    public void setRightCosplay(Cosplay rightCosplay) {
        this.rightCosplay = rightCosplay;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}