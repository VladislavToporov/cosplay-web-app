package database.models;

import database.dao.Identified;
import helpers.service.UserService;

import java.util.List;


public class User implements Identified<Integer> {

    private String username;
    private String login;
    private String password;
    private String token;
    private String email;
    private Integer id;
    private int votesCount;
    private int votesSum;
    private double rating;
    private UserService userService;
    private List<Cosplay> cosplayList;

    public String getImage() {
        return image;
    }

    private String image;

    public List<Cosplay> getCosplayList() {
        return cosplayList;
    }

    public void setCosplayList(List<Cosplay> cosplayList) {
        this.cosplayList = cosplayList;
    }

    public User(String username, String login, String password, String email, String token) {
        this.username = username;
        this.login = login;
        this.password = password;
        this.token = token;
        this.email = email;
        this.votesCount = 0;
        this.votesSum = 0;

    }

    public double calcRating() {
        double rating = 2 * userService.avgCosplay(id) + userService.avgComment(id) / 3;
        return Double.parseDouble(String.format("%(.2f Default locale", rating));
    }


    public User() {
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getId() {
        return id;
    }


    @Override
    public String toString() {
        return username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLogin() {
        return login;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public void setImage(String image) {
        this.image = image;

    }
}
