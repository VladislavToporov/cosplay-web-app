package database.models;

import database.dao.Identified;

public class Message implements Identified<Integer> {
    private Integer id;
    private int chatID;
    private String content;

    public int getChatID() {
        return chatID;
    }

    public void setChatID(int chatID) {
        this.chatID = chatID;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Message() {}

    @Override
    public Integer getId() {
        return id;
    }
}